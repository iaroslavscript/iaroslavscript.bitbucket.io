
## General sensors

```
user@pc:~$ echo install dependency
user@pc:~$ sudo apt-get install -y lm-sensors
user@pc:~$ 
user@pc:~$ echo find sensors and populate /etc/modules
user@pc:~$ yes YES | sensors-detect
user@pc:~$ 
user@pc:~$ echo loading modules
user@pc:~$

user@pc:~$ sensors
```

## GPU Nvidia sensors

```
user@pc:~$ echo install dependency
user@pc:~$ sudo apt-get install -y nvidia-utils-DRIVERVER
user@pc:~$ 
user@pc:~$ echo detailed info
user@pc:~$ nvidia-smi -q -d temperature
user@pc:~$ 
user@pc:~$ echo just int value
user@pc:~$ nvidia-smi --query-gpu=temperature.gpu --format=csv,noheader
```

