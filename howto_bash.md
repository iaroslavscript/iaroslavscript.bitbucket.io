

### Color formatted text

	user@pc:~$ TEXT_RED='\033[0;31m'
	user@pc:~$ TEXT_GREEN='\033[0;32m'
	user@pc:~$ TEXT_NOCOLOR='\033[0m'
	user@pc:~$
	user@pc:~$ # -e is required
	user@pc:~$ echo -e "Status ${TEXT_RED}FAILED${TEXT_NOCOLOR}"
	user@pc:~$ printf "Status ${TEXT_GREEN}OK${TEXT_NOCOLOR}\n"
	Status <font color="red">FAILED</font>
	Status <font color="green">OK</font>
	user@pc:~$


### Check process running

	# required root
	root@pc:~# kill -0 $(cat /run/openvpn/client.pid) && echo still running


### Heredoc example

	# Use dash to ignore left spaces offset. But it works only for tabs
        cat <<- EOF >> out.txt
                This multiline text starts from
		the beginning of line.

		This "$var" is replaced while \$var is not.
        EOF

	# Use single quote to ignore variables
        cat << 'EOF' >> out.txt
        This $var is not replaced.
        EOF


### Execute multiple commands on behalf of user while being root

use heredoc syntax

	root@pc:~# su username << EOF
	cat ~/out.log
	mv ~/Downloads/data.zip /tmp
	EOF
	root@pc:~#

Left offset. Both variants are OK

	root@pc:~# su username << EOF
		# heredok without dash
		cat ~/out1.log
		touch ~/out2.log
	EOF
	root@pc:~#
	root@pc:~# su username <<- EOF
		# heredok with dash
		cat ~/out.log
		touch ~/out2.log
	EOF
	root@pc:~#

Be aware that bash options are not inherited

	root@pc:~# set -e
	root@pc:~# su username << EOF
	/bin/false
	echo This line is printed
	EOF
	root@pc:~#
	root@pc:~# su username << EOF
	set -e
	/bin/false
	echo This line is NOT printed
	EOF
	root@pc:~#

Nested heredoc and variable substitutions

	# Inner quoted 'EOFF' can't help with subtitution
	root@pc:~# su username <<- EOF
		cat <<- <b>'EOFF'</b> > ~/src/.envrc
			export GOPATH=/var/lib/go
			export PATH=$PATH:/snap/bin/go:$GOPATH/bin
		EOFF
	EOF
	root@pc:~#
	root@pc:~# cat ~username/src/.envrc
	export GOPATH=/data0/study/go
	export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin:/snap/bin/go:/bin
	root@pc:~#

	# using quoted 'EOF' is not enherited to inner EOFF
	root@pc:~# su username <<- <b>'EOF'</b>
		cat <<- EOFF > ~/src/.envrc
			export GOPATH=/var/lib/go
			export PATH=$PATH:/snap/bin/go:$GOPATH/bin
		EOFF
	EOF
	root@pc:~#
	root@pc:~# cat ~username/src/.envrc
	export GOPATH=/data0/study/go
	export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin:/snap/bin/go:/bin
	root@pc:~#

	# it's possible to use backslash escape symbol
	# but still we need to use quoted 'EOFF'
	root@pc:~# su username <<- EOF
		cat <<- <b>'EOFF'</b> > ~/src/.envrc
			export GOPATH=/var/lib/go
			export PATH=<b>\$</b>PATH:/snap/bin/go:<b>\$<b>GOPATH/bin
		EOFF
	EOF
	root@pc:~#
	root@pc:~# cat ~username/src/.envrc
	export GOPATH=/var/lib/go
	export PATH=$PATH:/snap/bin/go:$GOPATH/bin
	root@pc:~#


### Using grep in condition

grep util has option -q. According to man-page is't:
> -q, --quiet, --silent
> Quiet; do not write anything to standard output. Exit immediately with zero status if any match is found, even if an error was detected.

	ip address show eth0 | grep 192.168.0.1 -q error || echo Something wrong with ip interface

	if grep /var/log/some.log -q error ; then
		echo Some error found.
		exit 1
	fi

	if ! ip route list | grep -q default ; then
		echo Default route is required.
		exit 1
	fi
	

### Bash trauble away

When calling function or programm which change something always use default value
Do not hope that script will not be run under root.
Do not hope to bash error options like "set -euf -o pipefail". Someone or even yourself could add "set +u" few lines above.

For simplify error detection:
- Use value VAR_NOT_SET for strings.
- Use value Nan for digits comparation.
- Use value /dev/null/ for path. Prefer /dev/null/ to /dev/null. Because it's incorrect path by itself (null is not a directory).

	mkdir -p ${SRC_PATH:-/dev/null/}/build
	rm -rf ${BUILD_PATH:-/dev/null/}


### Check dir not empty
	
	[ "$(ls -1 ${SOME_DIR:-/dev/null/} | wc -l)" -gt "0" ]  echo NOT EMPTY


### Check dir read-only (mount, bind or permitions)

This decion is weak and error prone.

	! touch ${CFG_NASKA_MOUNT_RO_DIR:-/dev/null/}/testtesttest > /dev/null 2>&1 && echo READ ONLY	


### END
	rsync -asv --port=${CFG_SERVER_PORT:-65000} --times --no-perms --no-owner --no-group --delete --exclude=lost+found \
		"${CFG_NASKA_MOUNT_RO_DIR}/$IN_DIR/" "${CFG_SERVER_IP:-254.254.254.254}::$IN_MODULE" >> "$LOG_FILENAME" 2>&1
	
	# form local sync use:
	# set -o pipefail
	# rsync -asv --no-perms --no-owner --no-group --delete --exclude=lost+found "{CFG_NASKA_MOUNT_RO_DIR}/$2" "/media/tu/HDD_2TB/copy_nas/" 2>&1 | tee -a "$LOG_FILENAME"
	# set +o pipefail

	local ERROR_CODE=$?
	
function openvpn_test_telnet {
	timeout --signal=9 3 bash -c 'exec 3<> /dev/tcp/'ya.ru'/'4434'' > /dev/null 2>&1

}

function naska_ping {
	
	local MSG='test naska reachable'

	echo 'ping naska [{}] ...'
	
	if ping -c 10 192.168.5.157 > /dev/null 2>&1 ; then
		echo -e "$MSG ${TEXT_GREEN}OK${TEXT_NC}"
	else
		echo -e "$MSG ${TEXT_RED}FAILED${TEXT_NC}"
                echo -e "${TEXT_RED}ERROR naska is not reachable. Check it alive. Check switch. Check network settings.${TEXT_NC}"          
                exit 1
	fi
}

function naska_mount {

	local MSG='test mounting volume'

	if mkdir -p ${CFG_NASKA_MOUNT_RO_DIR} && mount ${CFG_NASKA_MOUNT_RO_DIR} ; then
		echo -e "$MSG ${TEXT_GREEN}OK${TEXT_NC}"
	else
		echo -e "$MSG ${TEXT_RED}FAILED${TEXT_NC}"
		echo -e "${TEXT_RED}ERROR during mounting volume. See errors above.${TEXT_NC}"          
        	exit 1
	fi
}

function validate_task_line {

	local IN_FILE=$1

		line=$(echo $line | sed 's/[\ \t]\+/ /g')

		if echo $line | grep -qe '^\ *#' || echo $line | grep -q '^$' ; then
		fi

		if echo $line | grep -qve '[a-zA-Z0-9_]\+ [a-zA-Z0-9_\/]\+ [a-z0-9]\+' ; then
			echo "wrong task format. Only A-zA-Z0-9_ symbols are allowed"
		fi

		VAR_MODULE=$(echo $line | cut -f1 -d ' ')
		VAR_PATH=$(echo $line | cut -f2 -d ' ')
		VAR_SHA256=$(echo $line | cut -f3 -d ' ')

		if [[ ! "x$VAR_PATH" =~ ^x\./.+?[^/]$ ]] ; then
			echo "wrong task format. Path have to start with './' and have no '/' at the end"
		fi

		if [ "x$(echo "$VAR_MODULE=$VAR_PATH" | shasum -a 256 - | cut -f1 -d ' ')" != "x${VAR_SHA256:-0}" ] ; then
			echo "wrong task shasum"
		fi

		echo -e "$MSG ${TEXT_RED}FAILED${TEXT_NC}"
		echo -e "${TEXT_RED}ERROR task file wrong format. See message above.${TEXT_NC}"

}

function validate_task_file {

	local IN_FILE=$1
	local LINE_NO=0
	local TASK_TO_SYNC=0
	local LINES_MAX=1000
	local ERRORS_TOTAL=0
	local ERRORS_MAX=100
	local MSG='test task file valid'

	echo "validating task file [$IN_FILE] ..."

	# we can't pipe while because it creates inner shell and we couldn't change outer variables like LINE_NO
	# read (-r means no convert backslash)
	# last check is needed because read ignore last line if it does not have \n
	while IFS='' read -r line || [[ -n "$line" ]]; do

		LINE_NO=$((LINE_NO+1))

		if [ "${ERRORS_TOTAL:-Nan}" -ge "${ERRORS_MAX:-Nan}" ] ; then
			echo "too much errors at file."
                        break
		fi

		if [ "${LINE_NO:-Nan}" -ge "${LINES_MAX:-Nan}" ] ; then
			ERRORS_TOTAL=$((ERRORS_TOTAL+1))
			echo "file exceeds max file size of $LINES_MAX lines."
                        break
		fi

		line=$(echo $line | sed 's/[\ \t]\+/ /g')

		if echo $line | grep -qe '^\ *#' || echo $line | grep -q '^$' ; then
			continue
		fi

		if echo $line | grep -qve '[a-zA-Z0-9_]\+ [a-zA-Z0-9_\/]\+ [a-z0-9]\+' ; then
			echo "wrong format at line ${LINE_NO}"
			ERRORS_TOTAL=$((ERRORS_TOTAL+1))
			continue
		fi

		VAR_MODULE=$(echo $line | cut -f1 -d ' ')
		VAR_PATH=$(echo $line | cut -f2 -d ' ')
		VAR_SHA256=$(echo $line | cut -f3 -d ' ')

		if [ "x$(echo "$VAR_MODULE=$VAR_PATH" | shasum -a 256 - | cut -f1 -d ' ')" != "x${VAR_SHA256:-0}" ] ; then
			echo "wrong shasum at line ${LINE_NO}"
			ERRORS_TOTAL=$((ERRORS_TOTAL+1))
		fi

		TASK_TO_SYNC=$((TASK_TO_SYNC+1))

	done < "${IN_FILE:-/dev/null/}"

	if [ "${ERRORS_TOTAL:-Nan}" -ne "0" ] ; then
		echo -e "$MSG ${TEXT_RED}FAILED${TEXT_NC}"
		echo -e "${TEXT_RED}ERROR task file wrong format. See message above.${TEXT_NC}"
		exit 1
	fi

	echo -e "$MSG ${TEXT_GREEN}OK${TEXT_NC}"

	GLOBAL_TASKS=$TASK_TO_SYNC
}


function sync_tasks {

	local IN_FILE=$1
	local foo=$2

	echo "syncing tasks from file [$IN_FILE] ..."

	# we can't pipe while because it creates inner shell and we couldn't change outer variables like LINE_NO
	# read (-r means no convert backslash)
	# last check is needed because read ignore last line if it does not have \n
	while IFS='' read -r line || [[ -n "$line" ]]; do

		line=$(echo $line | sed 's/[\ \t]\+/ /g')

		if echo $line | grep -qe '^\ *#' || echo $line | grep -q '^$' ; then
			continue
		fi

		VAR_MODULE=$(echo $line | cut -f1 -d ' ')
		VAR_PATH=$(echo $line | cut -f2 -d ' ')
		VAR_SHA256=$(echo $line | cut -f3 -d ' ')

		if [ "x$(echo "$VAR_MODULE=$VAR_PATH" | shasum -a 256 - | cut -f1 -d ' ')" != "x${VAR_SHA256:-0}" ] ; then
			echo -e "${TEXT_RED}ERROR runtime wrong shasum after validation. line ${LINE_NO}${TEXT_NC}"
			exit 1
		fi

		MSG="YYYY.mm.dd HH:MM:SS Starting sync ${VAR_PATH:-NONAME}${VAR_PATH:-NONAME}${TEXT_NC} in ${TEXT_YELLOW}00${TEXT_NC} sec ..."
		# need to know string size for clearing after \r
		MSG_LEN=${#MSG}

		for i in $(seq 30 -1 1) ; do
			printf "$(date '+%Y.%m.%d %H:%M:%S') Starting sync ${VAR_PATH:-NONAME} in ${TEXT_YELLOW}%02d${TEXT_NC} sec ...\r" $i
			sleep 1
		done

		local myString=$(printf "%${MSG_LEN}s")
		local timestamp=$(date '+%Y.%m.%d %H:%M:%S')

		# print $MSG_LEN spaces to clear screen line.
		printf "${myString// /\ }\r"
		printf "$timestamp Run sync ${VAR_PATH:-NONAME} ...\r"

		# wrapped to extra shell so that Ctrl+C can't suppress printf
		SECONDS=0 ; ( $foo "${VAR_MODULE:-NONAME}" "${VAR_PATH:-/dev/null/}" ) \
			&& MSG_STATUS=${TEXT_GREEN}OK${TEXT_NC} || MSG_STATUS=${TEXT_RED}FAILED${TEXT_NC}

		# print $MSG_LEN spaces to clear screen line.
		printf "${myString// /\ }\r"
		printf "$timestamp Sync done ${TEXT_YELLOW}${VAR_PATH:-NONAME}${TEXT_NC} Time elapsed: $(sec_to_human $SECONDS) ${MSG_STATUS}\n"

	done < "${IN_FILE:-/dev/null/}"
}


function sec_to_human {
	
	printf "%02d:%02d:%02d" "$(( ${1} / 3600 ))" "$(( (${1} / 60) % 60 ))" "$(( ${1} % 60 ))"
}


if [ -z ${CLEAR_INSTALL_INIT+x} ]; then
	echo 'ERROR This file should not be run by it self'
	exit 1
fi

