
# Latest articles


---


### [Ansible. Passing variable to role](https://iaroslavscript.bitbucket.io/ba10002-ansible_passing_variable_to_role.html)

**Published: 2020-05-18**

This article shows how to pass a variable to a role or redefine a default
value. Also it clears up why example from ansible documentation gives a wrong
results and why you should avoid using `roles` statement.


---


### [Bash. Tips and Tricks](https://iaroslavscript.bitbucket.io/ba10001-bash_tips_and_tricks.html)

**Published: 2020-04-25**

This article is a collection of Bash Tips and Tricks. 


---

