[//]: # (title: Bash. Tips and Tricks)
[//]: # (id: ba10001)
[//]: # (publish: 2020-04-25)
[//]: # (update: 2020-04-25)
[//]: # (license: MIT)


# Bash. Tips and Tricks.


**Published: 2020-04-25**

This article is a collection of Bash Tips&Tricks.


### Color formatted text

<pre>
user@pc:~$ TEXT_RED='\033[0;31m'
user@pc:~$ TEXT_GREEN='\033[0;32m'
user@pc:~$ TEXT_NOCOLOR='\033[0m'
user@pc:~$
user@pc:~$ # -e is required
user@pc:~$ echo -e "Status ${TEXT_RED}FAILED${TEXT_NOCOLOR}"
user@pc:~$ printf "Status ${TEXT_GREEN}OK${TEXT_NOCOLOR}\n"
Status <font color="red">FAILED</font>
Status <font color="green">OK</font>
user@pc:~$
</pre>


### Check process running

- __First approach is required root permition.

	root@pc:~# kill -0 $(cat /run/openvpn/client.pid) && echo still running

- Using ps utility. No root required

	user@pc:~$ ps q $(cat /run/openvpn/client.pid) > /dev/null && echo still running
  	
where **q** option means:
  	
	q | -q | --quick-pid Select by PID (quick mode).  This selects the processes whose process ID numbers appear in pidlist.  With this option ps reads the necessary info only for the pids listed in the pidlist and doesn't apply additional filtering rules. The order of pids is unsorted and preserved. No additional selection options, sorting and forest type listings are allowed in this mode.

Use **p | -p | --pid** option if it's needed extra filtering or sorting features.
	
	user@pc:~$ ps p $(cat /run/openvpn/client.pid) > /dev/null && echo still running
	

### Heredoc example

	# Use dash to ignore left spaces offset. But it works only for tabs
	# Markdown converter replaces tabs with spaces!
		cat <<- EOF >> out.txt
		This multiline text starts from
		the beginning of line.

		This "$var" is replaced while \$var is not.
        EOF

	# Use single quote to ignore variables
	cat << 'EOF' >> out.txt
	This $var is not replaced.
	EOF


### Execute multiple commands on behalf of user while being root

I like to use heredoc syntax.
Remember that markdown converter replaces tabs with spaces!

	root@pc:~# su username << EOF
	cat ~/out.log
	mv ~/Downloads/data.zip /tmp
	EOF
	root@pc:~#

**Left offset. Both variants are OK**

	# Markdown converter replaces tabs with spaces!
	root@pc:~# su username << EOF
		# heredoc without dash
		cat ~/out1.log
		touch ~/out2.log
	EOF
	root@pc:~#
	root@pc:~# su username <<- EOF
		# heredoc with dash
		cat ~/out.log
		touch ~/out2.log
	EOF
	root@pc:~#

**Be aware that bash options are not inherited**

	# Markdown converter replaces tabs with spaces!
	root@pc:~# set -e
	root@pc:~# su username << EOF
	/bin/false
	echo This line is printed
	EOF
	root@pc:~#
	root@pc:~# su username << EOF
	set -e
	/bin/false
	echo This line is NOT printed
	EOF
	root@pc:~#

**Nested heredoc and variable substitutions**

Inner quoted 'EOFF' can't help with substitution.

	root@pc:~# su username <<- EOF
		cat <<- 'EOFF' > ~/src/.envrc
			export GOPATH=/var/lib/go
			export PATH=$PATH:/snap/bin/go:$GOPATH/bin
		EOFF
	EOF
	root@pc:~#

Let's see result.

	root@pc:~# cat ~username/src/.envrc
	export GOPATH=/data0/study/go
	export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin:/snap/bin/go:/bin
	root@pc:~#

Using quoted 'EOF' is not enherited to inner EOFF.

	root@pc:~# su username <<- 'EOF'
		cat <<- EOFF > ~/src/.envrc
			export GOPATH=/var/lib/go
			export PATH=$PATH:/snap/bin/go:$GOPATH/bin
		EOFF
	EOF
	root@pc:~#

Let's see result.

	root@pc:~# cat ~username/src/.envrc
	export GOPATH=/data0/study/go
	export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin:/snap/bin/go:/bin
	root@pc:~#

It's possible to use backslash escape symbol but still we need to use quoted 'EOFF'

	root@pc:~# su username <<- EOF
		cat <<- 'EOFF' > ~/src/.envrc
			export GOPATH=/var/lib/go
			export PATH=\$PATH:/snap/bin/go:\$GOPATH/bin
		EOFF
	EOF
	root@pc:~#

And now the correct result.

	root@pc:~# cat ~username/src/.envrc
	export GOPATH=/var/lib/go
	export PATH=$PATH:/snap/bin/go:$GOPATH/bin
	root@pc:~#


### Using grep in condition

grep util has option -q. According to man-page isn't:
> -q, --quiet, --silent
> Quiet; do not write anything to standard output. Exit immediately with zero status if any match is found, even if an error was detected.

	ip address show eth0 | grep 192.168.0.1 -q error || echo Something wrong with ip interface

	if grep /var/log/some.log -q error ; then
		echo Some error found.
		exit 1
	fi

	if ! ip route list | grep -q default ; then
		echo Default route is required.
		exit 1
	fi


### Bash trouble away

When calling function or program which change something always use default value
Do not hope that script will not be run under root.
Do not hope to bash error options like "set -euf -o pipefail". Someone or even yourself could add "set +u" few lines above.

For simplify error detection:
- Use value VAR_NOT_SET for strings.
- Use value Nan for comparing numbers.
- Use value /dev/null/ for path. Prefer /dev/null/ to /dev/null. Because it's incorrect path by itself (null is not a directory).

	systemctl restart ${SERVICE_NAME:-VAR_NOT_SET}
	[ ${i:-Nan} -gt 0 ]
	mkdir -p ${SRC_PATH:-/dev/null/}/build
	rm -rf ${BUILD_PATH:-/dev/null/}

### Check dir not empty

	[ "$(ls -1 ${SOME_DIR:-/dev/null/} | wc -l)" -gt "0" ]  echo NOT EMPTY


### Check dir read-only (mount, bind or permissions)

This decision is weak and error prone.

	! touch ${SOME_DIR:-/dev/null/}/testtesttest > /dev/null 2>&1 && echo READ ONLY	

