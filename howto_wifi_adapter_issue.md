
lshw -class Network
lspci | grep -i wireless

# more detailed info
lspci -knn | grep -i wireless -A 5

# network info
networkctl status wlo1

